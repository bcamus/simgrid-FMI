#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

// SYSTEM'S STATE

const double max_sim_time = 100;
const double sampling_time = 0.001;

// UTILITY
std::ofstream out;

static void checkResults(){
        double t = simgrid::s4u::Engine::get_clock();
        double x = simgrid::fmi::FMIPlugin::getRealOutput("lorenz_x","x");
        double y = simgrid::fmi::FMIPlugin::getRealOutput("lorenz_y","y");
        double z = simgrid::fmi::FMIPlugin::getRealOutput("lorenz_z","z");
        out << t << ";" << x << ";" << y << ";" << z << "\n";
}

static void sampler(std::vector<std::string>){
	XBT_INFO("perform co-simulation of Lorenz system until time");
	while(simgrid::s4u::Engine::get_clock() < max_sim_time){
		checkResults();
		simgrid::s4u::this_actor::sleep_for(sampling_time);
	}
	checkResults();
	out.close();
	XBT_INFO("co-simulation of Lorenz system done ! see you !");
}


// MAIN

int main(int argc, char *argv[])
{

  // SIMGRID INIT
  simgrid::s4u::Engine e(&argc, argv);

  const double intstepsize = std::stod(argv[1]);
  simgrid::fmi::FMIPlugin::initFMIPlugin(intstepsize);


  e.load_platform("../../platforms/clusters_rennes.xml");

  std::vector<std::string> args;

  // ADDING FMUs

  std::string fmu_uri = "lorenz_x/lorenz_x.fmu";
  std::string fmu_name = "lorenz_x";
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name);

  std::string fmu_uri2 = "lorenz_y/lorenz_y.fmu";
  std::string fmu_name2 = "lorenz_y";
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri2, fmu_name2);

   std::string fmu_uri3 = "lorenz_z/lorenz_z.fmu";
   std::string fmu_name3 = "lorenz_z";
   simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri3, fmu_name3);

  // CONNECTING FMUS

  simgrid::fmi::FMIPlugin::connectFMU("lorenz_x","x","lorenz_y","x");
  simgrid::fmi::FMIPlugin::connectFMU("lorenz_x","x","lorenz_z","x");
  simgrid::fmi::FMIPlugin::connectFMU("lorenz_y","y","lorenz_x","y");
  simgrid::fmi::FMIPlugin::connectFMU("lorenz_y","y","lorenz_z","y");
  simgrid::fmi::FMIPlugin::connectFMU("lorenz_z","z","lorenz_y","z");

  // LOG OUTPUT
  out.open("output.csv", std::ios::out);
  out << "\"time\",\"x\",\"y\",\"z\"\n";

  std::vector<port> ports_to_monitor = {
		  {"lorenz_x","x"},
		  {"lorenz_y","y"},
  	  	  {"lorenz_z","z"}};

  //simgrid::fmi::FMIPlugin::configureOutputLog("output.csv", ports_to_monitor);

  simgrid::fmi::FMIPlugin::readyForSimulation();

  //simgrid::fmi::FMIPlugin::on_state_change.connect(checkResults);

  // CREATING SIMGRID ACTORS

  simgrid::s4u::Actor::create("waiting_actor", simgrid::s4u::Host::by_name("c-0.rennes"), sampler, args);

  e.run();

}
