#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

// SYSTEM'S STATE

const double max_sim_time = 100;
const double sampling_time = 0.001;

// UTILITY

std::ofstream out;

static void checkResults(){
        double t = simgrid::s4u::Engine::get_clock();
        double x = simgrid::fmi::FMIPlugin::getRealOutput("lotka_volterra_x","x");
        double y = simgrid::fmi::FMIPlugin::getRealOutput("lotka_volterra_y","y");
        out << t << ";" << x << ";" << y << "\n";
}

static void sampler(std::vector<std::string>){
	XBT_INFO("perform co-simulation of Lotka-Volterra system");
	checkResults();
	while(simgrid::s4u::Engine::get_clock() < max_sim_time){
		simgrid::s4u::this_actor::sleep_for(sampling_time);
		checkResults();
	}
	out.close();
	XBT_INFO("co-simulation of Lotka-Volterra system done ! see you !");
}

// MAIN

int main(int argc, char *argv[])
{

  out.open("output.csv", std::ios::out);
  out << "\"time\",\"x\",\"y\"\n";

  // SIMGRID INIT
  simgrid::s4u::Engine e(&argc, argv);

  const double intstepsize = (argc>=4)? std::stod(argv[3]) : 0.001;
  simgrid::fmi::FMIPlugin::initFMIPlugin(intstepsize);


  e.load_platform("../../platforms/clusters_rennes.xml");

  std::vector<std::string> args;

  // ADDING FMUs

  std::string fmu_uri = (argc>=2)? argv[1] : "lotka_volterra_x.fmu";
  std::string fmu_name = "lotka_volterra_x";

  std::string fmu_uri2 = (argc>=3)? argv[2] : "lotka_volterra_y.fmu";
  std::string fmu_name2 = "lotka_volterra_y";

  XBT_INFO("loading FMUs %s and %s", fmu_uri.c_str(), fmu_uri2.c_str());

  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name,false);
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri2, fmu_name2,false);

  // CONNECTING FMUS

  simgrid::fmi::FMIPlugin::connectFMU("lotka_volterra_x","x","lotka_volterra_y","x");
  simgrid::fmi::FMIPlugin::connectFMU("lotka_volterra_y","y","lotka_volterra_x","y");

  simgrid::fmi::FMIPlugin::readyForSimulation();

  // CREATING SIMGRID ACTORS

  simgrid::s4u::Actor::create("sampling_actor", simgrid::s4u::Host::by_name("c-0.rennes"), sampler, args);

  e.run();

}
