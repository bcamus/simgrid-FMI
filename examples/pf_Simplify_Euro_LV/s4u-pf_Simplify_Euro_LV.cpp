#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <math.h>
#include <vector>
#include <list>
#include <unordered_map>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

/**
* In this use case, we load an FMU exported with PowerFactory-FMU.
*/

/**
* SIMUALTION PARAMETERS
**/

// start time of the simulation
double start_time = 63000;
// duration of the simulation
double simulation_length = 4500;
// sampling period of the simulation output
double continuous_output_step_size = 1;
// type of architecture deployed (0 = no cascado-cyclic, 1 = centralized, 2 = decentralized)
int archi_type = 1;
// maximum cumulative duraton allowed for a heater
double max_cumulative_shutdown_duration = 10 * 60;

/**
* ACTOR PARAMETERS
**/

// current (in kA) in LINE1 above wich the cascado-cyclical process should be started
double ligne1_current_threshold = 0.430;
// current (in KA) in LINE1 below which the cascado-cyclical process should be stopped
double lower_current_threshold = 0.400;

// sampling period of the heaters probes
double power_sampling_period = 1;
// sampling period of the LINE1 probe
double current_sampling_period = 1;

// sliding window size for the centralize master to compute the average power consumption of each heater (the window spans a period approximately equivalent to avg_sliding_window_size * power_sampling_period
int avg_sliding_window_size = 300;

// size (in bytes) of a message send by the probe of a heater
double sampler_message_size = 1024;
// size (in bytes) of a message send from the master to a heater
double control_message_size = 1024;
// size of an acknowledgement message send by the heater
double ack_message_size = 1024;

// maximum shutdown duration of a heater in the cascado-cyclical process
double max_shutdown_time = 60;

// number of controlable heaters
int nb_heaters = 15;
// consumption of a heater (in kW)
double heater_consumption = 2;
//number of heaters per household
int nb_heaters_per_household = 3;
// the number of shutdown to perform when the cascado-cyclic process is running
int nb_shutdowns;

/**
* GLOBALS
**/

// the heaters names (which corresponds both the controller name and the FMU input ports ID)
std::vector<std::string> heaters_names;
// the heaters consumptions
std::unordered_map<std::string, std::list<double>> heaters_consumptions;
// the heaters that can be shutdown sorted by priorities order
std::vector<std::string> shutdown_priorities;
// the heaters currently shutdown
std::set<std::string> shutdown_heaters;
// the cascado cyclic manager
simgrid::s4u::ActorPtr cascado_cyclic_actor;
// the timer actor use in the centralized architecture to manage the shutdown time of the heaters
simgrid::s4u::ActorPtr timer_actor;
// the actor used in the centralized architecture to manage the activation and deactivation of cascado-cyclic process
simgrid::s4u::ActorPtr alarm_actor;
// indicate wether the timer_actor is running or not
bool sleeping;
// indicate if the centralized cascado_cyclic manager should start a new iteration or add new heaters in the current iteration
bool new_shutdown;

// the current in Line1 as known by the master
double line1_current;
// the number of shutdown cycles completely performed
int nb_cycles;
// used in the decentralized algorithm to be sure that a token from a previous cascado_cyclic process can not be forwarded
bool synchronization_needed;
// indicate wether a decentralized shutdown process is currently running for a heater
std::unordered_map<int, bool> decentralized_shutdown_is_running;
// cumulative shutdown duration per heater (used to be sure that the cumulative duration does not exceed max_cumulative_duration_allowed)
std::unordered_map<std::string, double> cumulative_shutdown_durations;
// times of the last shutdown start for each heater (used to compute the cumulative shutdown duration)
std::unordered_map<std::string, double> last_shutdown_start_time;
// the number of shutdown performed on each heaters
std::unordered_map<std::string, int> heater_nb_shutdowns;
// ID of the mailbox used to send probe data to the controller
std::string master_mailbox_id = "master_mailbox";

/**
* FMU PARAMETERS
**/

// name of the PowerFactory FMU
std::string fmu_name = "proxy_fmu";
// URI of the FMU
std::string fmu_uri = "";
// step size used by SimGrid to synchronize the FMU
double step_size = 0.1;

// name of the output port used to retrieve the current in LINE1 (in kA)
std::string line1_current_output_id = "ElmLne.LINE1.m:I:bus1:A";
// prefix and suffix of the output port used to retrieve the power consumption of a house (in W). The port ID equals prefix + house ID + suffix.
std::string P_sum_output_id_prefix = "ElmLod.LOAD";
std::string P_sum_output_id_suffix = ".m:Psum:bus1";
// prefix and suffix of the input port used to activate/deactivate the heater of a house. The port ID equals prefix + house ID + suffix.
std::string controller_input_prefix = "EvtParam.Cons_";
std::string controller_input_suffix = ".SignalActivation";
// prefix and suffix of the output used to retrieve the power consumption of a heater (in W). The port ID equals prefix + heater ID + suffix.
std::string P_heater_output_id_prefix = "ElmDsl.P_heater";
std::string P_heater_output_id_suffix = ".s:P_heater";


/*
* OUTPUT
**/

// last time the current crossed the upper threshold
double last_crossing_time;
// cumulative time spend above the upper threshold
double total_peaks_duration;
// max intensity of each peak
double max_peak_intensity;
// status of the heaters (i.e. 1=activated, 0=deactivated)
std::unordered_map<std::string,int> heaters_status;
// number of messages sent
int nb_messages_sent = 0;


// output stream to log the cumulative time spent above the upper threshold
std::ofstream out_total_peaks_duration;
// output stream to log the heaters power consumption
std::ofstream out_P_heater;
// output stream to log the power consumption of the houses
std::ofstream out_P_sum;
// output stream to log the current in LINE1
std::ofstream out_I_Line1;
// output stream to log the status of the heaters
std::ofstream out_heaters_status;
// output stream to log the cascado-cyclic process state
std::ofstream out_cascado_cyclic_status;
// output stream to log the cascado-cyclic process iterations
std::ofstream out_cascado_cyclic_iterations;
// output stream to log the number of messages sent
std::ofstream out_nb_messages_sent;
// output stream to log the lower threshold variations and the number of household shaded
std::ofstream out_lower_threshold;

/**
* DATA TYPES
**/

// messages send from the probes to the master
typedef struct{
  std::string name;
  double value;
  double time;
} sampler_data;

/**
* UTILITY
**/

static void log_continuous_outputs(){

  double time = simgrid::s4u::Engine::get_clock();
  double current = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, line1_current_output_id);

  out_P_heater << time;
  out_P_sum << time;
  out_I_Line1 << time << ";" << current << "\n";

  max_peak_intensity = std::max(max_peak_intensity, current);

  for(int i=1;i<=nb_heaters;i++){
    std::string P_sum_id = P_sum_output_id_prefix + std::to_string(i) + P_sum_output_id_suffix;
    std::string P_heater_id = P_heater_output_id_prefix + std::to_string(i) + P_heater_output_id_suffix;

    out_P_sum << ";" << simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, P_sum_id);
    //out_P_heater << ";" << simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, P_heater_id);
  }

  out_P_heater << "\n";
  out_P_sum << "\n";
}

static void log_nb_messages_sent(simgrid::kernel::resource::NetworkAction&, simgrid::kernel::resource::Action::State state){
  XBT_DEBUG("log message");
  if(state == simgrid::kernel::resource::Action::State::STARTED){
    nb_messages_sent++;
    out_nb_messages_sent << simgrid::s4u::Engine::get_clock() << ";" << nb_messages_sent << "\n";
  }
  XBT_DEBUG("done logging message");
}

// used to detect current peaks in the simulation log
static bool reactOnThresholdCrossing(std::vector<std::string> params){
  double x = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, line1_current_output_id);
  return (params[0]=="above")? (x >= ligne1_current_threshold) : (x < ligne1_current_threshold);
}

// used to count the time spend above the threshold in the simulation log
static void recordPeaks(std::vector<std::string> params){   

  std::string new_param = "below";
  double current_time = simgrid::s4u::Engine::get_clock();

  if(params[0] == "below"){
    total_peaks_duration += current_time - last_crossing_time;
    new_param = "above";
  }else{
    max_peak_intensity = 0;
  }

  last_crossing_time = current_time;
  simgrid::fmi::FMIPlugin::registerEvent(reactOnThresholdCrossing, recordPeaks, {new_param});

  out_total_peaks_duration << last_crossing_time << ";" << total_peaks_duration << ";" << max_peak_intensity << "\n";
}

static double computeHeaterAvgCons(std::string heater_name){
  double avg_cons = 0;
  for(double cons : heaters_consumptions[heater_name])
    avg_cons += cons;

  return avg_cons / (double) heaters_consumptions[heater_name].size();
}

// used to sort the heaters in an decreasing consumption order
static bool compareHeaters(std::string h1, std::string h2){
  if(heaters_consumptions.find(h1) == heaters_consumptions.end())
    return false;

  if(heaters_consumptions.find(h2) == heaters_consumptions.end())
    return true;

  return computeHeaterAvgCons(h1) > computeHeaterAvgCons(h2);
}


static void end_of_simulation(){
  int cascado_cyclic_state = (cascado_cyclic_actor == nullptr || cascado_cyclic_actor->is_suspended())? 0 : 1;
  out_cascado_cyclic_status << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(cascado_cyclic_state) <<"\n";

  out_P_heater.close();
  out_P_sum.close();
  out_I_Line1.close();

  if(archi_type > 0){
    out_heaters_status.close();
    out_cascado_cyclic_status.close();
    out_cascado_cyclic_iterations.close();

    out_nb_messages_sent << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(nb_messages_sent) << "\n";
    out_nb_messages_sent.close();
    out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(lower_current_threshold) << ";" << std::to_string(nb_shutdowns) <<"\n";
    out_lower_threshold.close();
  }

  std::string param = (reactOnThresholdCrossing({"above"}))? "below" : "above";
  recordPeaks({param});
  out_total_peaks_duration.close();
}

static void log_controller_output(std::string name, double value){
  heaters_status[name] = value;

  // we compute the cumulative shutdown duration for the heater and check that it does not exceed the maximum allowed
  if(value == 0){
    last_shutdown_start_time[name] = simgrid::s4u::Engine::get_clock();
  }else{
    cumulative_shutdown_durations[name] += simgrid::s4u::Engine::get_clock() - last_shutdown_start_time[name];

    if(cumulative_shutdown_durations[name] > max_cumulative_shutdown_duration){
      XBT_CRITICAL("HEATER %s EXCEEDS THE MAXIMUM SHUTDOWN DURATION : %f", name.c_str(), cumulative_shutdown_durations[name]);
      end_of_simulation();
      exit(0);
    }
  }

  out_heaters_status << simgrid::s4u::Engine::get_clock();
  for(std::string id : heaters_names){
    out_heaters_status << ";" << heaters_status[id];
  }
  out_heaters_status << "\n";
}

/**
 * ACTORS BEHAVIORS
 */

// Centralized version
static void sampler(std::string var_name, std::string load_name, double sampling_period){

  sampler_data data;
  data.name = load_name;
  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(master_mailbox_id);

  XBT_INFO("start sampling %s", var_name.c_str());
  while(true){
    simgrid::s4u::this_actor::sleep_for(sampling_period);
    data.time = simgrid::s4u::Engine::get_clock();
    data.value = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, var_name);
    XBT_DEBUG("sending data { name = %s , value = %f, time = %f}", data.name.c_str(), data.value, data.time);
    mailbox->put(&data, sampler_message_size);
  }	
}


static void controller(std::string input_name, std::string load_name){

  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(load_name);

  XBT_INFO("start controlling %s", input_name.c_str());
  while(true){
    int command = *((int*) mailbox->get());
    XBT_INFO("applying the command : %i", command);
    log_controller_output(load_name, command);
    simgrid::fmi::FMIPlugin::setRealInput(fmu_name, input_name, command);
  }
}

static void timer(){
  sleeping = true;
  simgrid::s4u::this_actor::sleep_for(max_shutdown_time);
  sleeping = false;
}

static void cascado_cyclic_alarm(){
  timer_actor->join();
}

static void cascado_cyclic_manager(){

   int start_shutdown = 0;
   int stop_shutdown = 1;

   nb_cycles = 0;

   sleeping = false;
   new_shutdown = false;

   while(true){
      // check if we start a new iteration or if we simply must add new shutdown in the current iteration
      if(!new_shutdown){
        // send the stop shutdown command
        for(std::string target : shutdown_heaters){
          XBT_INFO("sending stop shutdown command to heater %s", target.c_str());
          simgrid::s4u::CommPtr comm = simgrid::s4u::Mailbox::by_name(target)->put_init(&stop_shutdown, control_message_size);
          comm->detach();
        }

        out_cascado_cyclic_iterations << simgrid::s4u::Engine::get_clock() << "\n";

        // if we don't have shutdown to perform, then sleep. The master will wake us up when needed.
        if(line1_current <= lower_current_threshold){
          XBT_INFO("stop cascado-cyclic process because the current in LINE 1 is sufficiently low (%f)",line1_current);
          out_cascado_cyclic_status << simgrid::s4u::Engine::get_clock() << ";0\n";
          simgrid::s4u::this_actor::suspend();
          out_cascado_cyclic_status << simgrid::s4u::Engine::get_clock() << ";1\n";
        }

        // we start a timer in a dedicated actor.
        timer_actor = simgrid::s4u::Actor::create("timer", simgrid::s4u::this_actor::get_host(), timer);
        timer_actor->daemonize();      

        shutdown_heaters.clear();

      }else{
        new_shutdown = false;
      }

      // we start an alarm actor that wait for the timer to finish. The master can kill this actor to wake us up sooner.
      alarm_actor = simgrid::s4u::Actor::create("alarm", simgrid::s4u::this_actor::get_host(), cascado_cyclic_alarm);
      alarm_actor->daemonize();

      // we search the target according to their priority
      auto it = shutdown_priorities.begin();
      std::vector<simgrid::s4u::CommPtr> shutdown_comms;

      while(shutdown_heaters.size() < nb_shutdowns && shutdown_heaters.size() < nb_heaters ){

        std::string heater = *it;
        // make sure that everybody is shutdown the same amount of times and that a heater can not be selected twice in the same cascado-cyclic iteration
        if(heater_nb_shutdowns[heater] == nb_cycles && shutdown_heaters.find(heater) == shutdown_heaters.end()){
          shutdown_heaters.insert(heater);
          heater_nb_shutdowns[heater]++;
          // send the shutdown command 
          XBT_INFO("sending shutdown command to heater %s", heater.c_str());
          shutdown_comms.push_back(simgrid::s4u::Mailbox::by_name(heater)->put_async(&start_shutdown, control_message_size));
        }
        // if we reach the end of the list, then all the heaters have been selected the same amount of time, and we start a new cycle.
        it++;
        if(it == shutdown_priorities.end()){
          it = shutdown_priorities.begin();
          nb_cycles++;
        }
      }

      // we wait for all the comms to be over in order to be sure that we do not simutaneously send shutdown and restart command to the heaters 
      simgrid::s4u::Comm::wait_all(&shutdown_comms);
            
      // wait for the timer to finish, or the master to wake us up.
      alarm_actor->join();
   }
}

static void updateNbShutdownAndLowerThreshold(){
  // compute the number of heaters to shutdown and the lower threshold
  double current_gap = line1_current - ligne1_current_threshold;
  double power_gap = current_gap * std::sqrt(3) * 400;
  // we consider the worst case scenario here -i.e. that the household consumption only equals the consumption of a single heater
  nb_shutdowns += (int) std::ceil(power_gap / heater_consumption);
  nb_shutdowns = std::min(nb_heaters, nb_shutdowns);
  // we also consider the worst case scenario to compute the lower threshold -i.e. that the household consumption equals the sum of the consumption of its heaters
  lower_current_threshold = ligne1_current_threshold - nb_shutdowns * heater_consumption * nb_heaters_per_household / (std::sqrt(3) * 400) ;
  out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(lower_current_threshold) << ";" << std::to_string(nb_shutdowns) << "\n";
}

static void master(){
  
  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(master_mailbox_id);

  lower_current_threshold = 99999999999999999;
  out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(ligne1_current_threshold) << ";" << std::to_string(nb_shutdowns) << "\n";

  nb_shutdowns = 0;

  int nb_reception = 0;
 
  while(true){
    
    sampler_data *probe_data = (sampler_data*) mailbox->get();
    nb_reception++;
    XBT_DEBUG("received %i data from a probe....", nb_reception);
    XBT_DEBUG("received data from probe %s, value = %f",probe_data->name.c_str(), probe_data->value);
    
    if(probe_data->name != line1_current_output_id){
       heaters_consumptions[probe_data->name].push_front(probe_data->value);
       if(heaters_consumptions[probe_data->name].size() > avg_sliding_window_size)
         heaters_consumptions[probe_data->name].pop_back();
       std::sort(shutdown_priorities.begin(), shutdown_priorities.end(), compareHeaters);
    }else{
      line1_current = probe_data->value;
      // check if we need to start new cascado-cyclic shutdowns
      if(line1_current >= ligne1_current_threshold){

         updateNbShutdownAndLowerThreshold();       
 
         if(cascado_cyclic_actor->is_suspended()){
          XBT_INFO("!!! too much current (%f kA) start cascado-cyclic process !!! ", line1_current);
          cascado_cyclic_actor->resume();
        }else{
          XBT_INFO("!!! too much current (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current);
          new_shutdown = true;
          alarm_actor->kill();
        } 
      }else if(line1_current <= lower_current_threshold && sleeping){
        sleeping = false;
        nb_shutdowns = 0;
        lower_current_threshold = 9999999999999999;
        out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(ligne1_current_threshold) << ";" << std::to_string(nb_shutdowns) << "\n";
        timer_actor->kill();
      }
    }
    XBT_DEBUG("done processing the data");
  }
}

// decentralized version
static void token_generator(int nb_tokens){

  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + "1" + controller_input_suffix);
  int token_id = nb_cycles;

  XBT_DEBUG("generating the tokens");
  for(int i=0; i < nb_tokens; i++)
    send_mailbox->put(&token_id, control_message_size);
  // to be sure that token_id is received before we leave
  simgrid::s4u::this_actor::yield();
}

static void decentralized_master(){

  int stop_cycle = -1;

  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + "1" + controller_input_suffix);

  XBT_INFO("start sampling %s", line1_current_output_id.c_str());

  bool cascado_cyclic_started = false;
  synchronization_needed = false;

  lower_current_threshold = 99999999999999999;
  nb_shutdowns = 0;

  out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(ligne1_current_threshold) << ";" << std::to_string(nb_shutdowns) << "\n";

  while(true){

    line1_current = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, line1_current_output_id);
    
    if(line1_current >= ligne1_current_threshold){

      int old_nb_shutdowns = nb_shutdowns;
      updateNbShutdownAndLowerThreshold();
      int nb_tokens = nb_shutdowns;

      if(!cascado_cyclic_started){
        XBT_INFO("!!! too much current (%f kA) start cascado-cyclic process !!! ", line1_current);
        out_cascado_cyclic_status << simgrid::s4u::Engine::get_clock() << ";1\n";
        cascado_cyclic_started = true;
      }else{
        XBT_INFO("!!! too much current (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current);
        nb_tokens -= old_nb_shutdowns; 
      }

      simgrid::s4u::ActorPtr gen = simgrid::s4u::Actor::create("token_generator", simgrid::s4u::this_actor::get_host(), token_generator, nb_tokens);
      gen->daemonize();

    }else if(line1_current <= lower_current_threshold && cascado_cyclic_started){

      XBT_INFO("stop cascado-cyclic process because the current in LINE 1 is sufficiently low (%f)", line1_current);
      out_cascado_cyclic_status << simgrid::s4u::Engine::get_clock() << ";0\n";
      cascado_cyclic_started = false;
      
      // send the stop message to the heaters
      synchronization_needed = true;

      nb_shutdowns = 0;
      lower_current_threshold = 9999999999999999;
      out_lower_threshold << simgrid::s4u::Engine::get_clock() << ";" << std::to_string(ligne1_current_threshold) << ";" << std::to_string(nb_shutdowns) << "\n";

      simgrid::s4u::CommPtr comm = mailbox->put_init(&stop_cycle, control_message_size);
      comm->detach();
    }

    simgrid::s4u::this_actor::sleep_for(current_sampling_period);
  }
}

static void decentralized_cycle_manager(){

  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + "1" + controller_input_suffix);
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(nb_heaters+1) + controller_input_suffix);

  // manage the cycles
  while(true){
    int token_id = *((int*) recv_mailbox->get());

    if(synchronization_needed){
      synchronization_needed = (token_id >= 0);
    }else{
      // detect new cycle
      if(token_id == nb_cycles)
         nb_cycles++;
      // if we do not receive a stop shutdown command, then forward the new token with the updated token
      if(token_id >= 0)
        send_mailbox->put(&nb_cycles, control_message_size);
    }
  }
}

static void shutdown_behavior(int i, int token_id){

  decentralized_shutdown_is_running[i] = true;

  std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix;
  std::string load_name = "LOAD_"+std::to_string(i);
  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(i+1) + controller_input_suffix);

  XBT_INFO("start applying the command : { command = 0, duration = %f }", max_shutdown_time);
  log_controller_output(load_name, 0.0);
  simgrid::fmi::FMIPlugin::setRealInput(fmu_name, controller_name, 0.0);
  simgrid::s4u::this_actor::sleep_for(max_shutdown_time);
  XBT_INFO("stop applying the command : { command = 0, duration = %f }", max_shutdown_time);
  log_controller_output(load_name, 1.0);
  simgrid::fmi::FMIPlugin::setRealInput(fmu_name, controller_name, 1.0);

  decentralized_shutdown_is_running[i] = false;

  send_mailbox->put(&token_id, control_message_size);
  // to be sure that the receivers get token_id before we leave
  simgrid::s4u::this_actor::yield();
}

static void decentralized_controller(int id){

  std::string input_name = controller_input_prefix + std::to_string(id) + controller_input_suffix;

  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(id+1) + controller_input_suffix);
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(input_name);

  int nb_shutdown = 0;
  simgrid::s4u::ActorPtr shutdown_process;
  decentralized_shutdown_is_running[id] = false;
  
  int token_id = 0;
  while(true){
    token_id = *((int*) recv_mailbox->get());
    
    if(token_id == nb_shutdown && !decentralized_shutdown_is_running[id]){
      nb_shutdown++;
      shutdown_process = simgrid::s4u::Actor::create("shutdown_process_"+std::to_string(id), simgrid::s4u::this_actor::get_host(), shutdown_behavior, id, token_id);
      shutdown_process->daemonize();
    }else{
      if(token_id < 0 && decentralized_shutdown_is_running[id]){
        shutdown_process->kill();
        decentralized_shutdown_is_running[id] = false;
        simgrid::fmi::FMIPlugin::setRealInput(fmu_name, input_name, 1.0);
        log_controller_output("LOAD_"+std::to_string(id), 1); 
      }
      send_mailbox->put(&token_id, control_message_size);
    }
  }
}

// global simulation behavior

static void deploy_actors(){

  simgrid::s4u::Engine *e = simgrid::s4u::Engine::get_instance();

  simgrid::s4u::ActorPtr current_actor = simgrid::s4u::Actor::create("master", e->get_all_hosts()[0], master);
  simgrid::s4u::Mailbox::by_name(master_mailbox_id)->set_receiver(current_actor);
  current_actor->daemonize();

  cascado_cyclic_actor = simgrid::s4u::Actor::create("cascado_cyclic_manager", e->get_all_hosts()[0], cascado_cyclic_manager);
  cascado_cyclic_actor->daemonize();

  current_actor = simgrid::s4u::Actor::create("sampler_"+line1_current_output_id, e->get_all_hosts()[nb_heaters+1], sampler, line1_current_output_id, line1_current_output_id, current_sampling_period);
  current_actor->daemonize();

  for(int i=1;i<=nb_heaters;i++){
    std::string sampler_name = P_sum_output_id_prefix + std::to_string(i) + P_sum_output_id_suffix;
    std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix;
    std::string load_name = "LOAD_"+std::to_string(i);

    current_actor = simgrid::s4u::Actor::create("sampler_"+sampler_name, e->get_all_hosts()[i+1], sampler, sampler_name, load_name, power_sampling_period);
    current_actor->daemonize();

    current_actor = simgrid::s4u::Actor::create("controller_"+controller_name, e->get_all_hosts()[i+1], controller, controller_name, load_name);
    current_actor->daemonize();

    heaters_names.push_back(load_name);
    shutdown_priorities.push_back(load_name);
    heaters_status[load_name] = 1.;
    heater_nb_shutdowns[load_name] = 0;
  }
}

static void deploy_decentralized_actors(){
  
  simgrid::s4u::Engine *e = simgrid::s4u::Engine::get_instance();
  simgrid::s4u::ActorPtr current_actor = simgrid::s4u::Actor::create("decentralied_master", e->get_all_hosts()[1], decentralized_master);
  current_actor->daemonize();

  cascado_cyclic_actor = simgrid::s4u::Actor::create("decentralized_cyclic_manager", e->get_all_hosts()[1], decentralized_cycle_manager);
  cascado_cyclic_actor->daemonize();
  
  for(int i=1;i<=nb_heaters;i++){
    std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix;
    std::string load_name = "LOAD_"+std::to_string(i);

    current_actor = simgrid::s4u::Actor::create("controller_"+controller_name, e->get_all_hosts()[i+1], decentralized_controller, i);
    current_actor->daemonize();

    heaters_names.push_back(load_name);
    heaters_status[load_name] = 1.;
    heater_nb_shutdowns[load_name] = 0;
  }

}

static void simulation_manager(){

  if(archi_type == 1 ){
    out_heaters_status.open("heaters_status.csv", std::ios::out);
    out_cascado_cyclic_status.open("cascado_cyclic_state.csv");
    out_cascado_cyclic_iterations.open("cascado_cyclic_iterations.csv");
    out_total_peaks_duration.open("total_peaks_duration.csv");
    out_I_Line1.open("I_Line1.csv", std::ios::out);
    out_P_heater.open("P_heater.csv", std::ios::out);
    out_P_sum.open("P_sum.csv", std::ios::out);
    out_nb_messages_sent.open("nb_messages_sent.csv");
    out_lower_threshold.open("lower_threshold.csv");
  }else if(archi_type == 2){
    out_heaters_status.open("decentralized_heaters_status.csv", std::ios::out);
    out_cascado_cyclic_status.open("decentralized_cascado_cyclic_state.csv");
    out_cascado_cyclic_iterations.open("decentralized_cascado_cyclic_iterations.csv");
    out_total_peaks_duration.open("decentralized_total_peaks_duration.csv");
    out_I_Line1.open("decentralized_I_Line1.csv", std::ios::out);
    out_P_heater.open("decentralized_P_heater.csv", std::ios::out);
    out_P_sum.open("decentralized_P_sum.csv", std::ios::out);
    out_nb_messages_sent.open("decentralized_nb_messages_sent.csv");
    out_lower_threshold.open("decentralized_lower_threshold.csv");
  }else{
    out_total_peaks_duration.open("total_peaks_duration_without_cascado_cyclic.csv");
    out_I_Line1.open("I_Line1_without_cascadocyclic.csv", std::ios::out);
    out_P_heater.open("P_heater_without_cascadocyclic.csv", std::ios::out);
    out_P_sum.open("P_sum_without_cascadocyclic.csv", std::ios::out);
  }

  simgrid::s4u::Link::on_communication_state_change.connect(log_nb_messages_sent);

  simgrid::s4u::this_actor::sleep_until(start_time);
   
  if(archi_type > 0)
    out_nb_messages_sent << std::to_string(start_time) << ";0\n";
     
  XBT_DEBUG("deploying the FMU");
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name, false);
  for(int i=1;i<=nb_heaters;i++){
    std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix;
    simgrid::fmi::FMIPlugin::setRealInput(fmu_name, controller_name, 1.0);
  }

  last_crossing_time = start_time;
  std::string param = (reactOnThresholdCrossing({"above"}))? "above" : "below";
  recordPeaks({param});

  XBT_DEBUG("deploying the actors");
  if(archi_type > 0){
    if(archi_type == 1){
      deploy_actors();
    }else{
      deploy_decentralized_actors();
    }
  }

  XBT_DEBUG("start observing simulation of Power Factory");
  log_continuous_outputs();
  while(simgrid::s4u::Engine::get_clock() <= start_time + simulation_length){
    simgrid::s4u::this_actor::sleep_for(continuous_output_step_size);
    XBT_DEBUG("logging output");
    log_continuous_outputs(); 
  }

  end_of_simulation();
}


int main(int argc, char *argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  simgrid::fmi::FMIPlugin::initFMIPlugin(step_size);

  archi_type = (argc >= 3)? std::stoi(argv[2]) : 1;
  nb_heaters = (argc >= 4)? std::stoi(argv[3]) : 30;
  ligne1_current_threshold = (argc >= 5)? std::stod(argv[4]) : 0.43;  

  XBT_INFO("architecture type = %i (0 = no cascado-cyclic, 1 = centralized, 2 = decentralized)", archi_type);
  XBT_INFO("number of households = %i ", nb_heaters);
  XBT_INFO("upper current threshold = %f", ligne1_current_threshold);


  e.load_platform("../../platforms/simplified_euro_lv.xml");
  
  simgrid::s4u::Actor::create("simulation_manager", e.get_all_hosts()[0], simulation_manager);

  fmu_uri = (argc>=2)? argv[1] : "file://power_factory";

  simgrid::fmi::FMIPlugin::readyForSimulation();
  e.run();

  return 0;
}
