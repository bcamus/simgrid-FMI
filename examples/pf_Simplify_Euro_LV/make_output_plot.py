import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys

nb_heaters = 20
upper_threshold = 0.42

plt.figure(figsize=(16,8))

decentralized_cascado_cyclic = np.genfromtxt("decentralized_I_Line1.csv", delimiter=';', names=['x','y'])
with_cascado_cyclic = np.genfromtxt('I_Line1.csv', delimiter=';', names=['x','y'])
without_cascado_cyclic = np.genfromtxt('I_Line1_without_cascadocyclic.csv', delimiter=';', names=['x','y'])
cascado_cyclic_state = np.genfromtxt('cascado_cyclic_state.csv', delimiter=';', names=['x','y'])
decentralized_cascado_cyclic_state = np.genfromtxt('decentralized_cascado_cyclic_state.csv', delimiter=';', names=['x','y'])
cascado_cyclic_iterations = np.genfromtxt('cascado_cyclic_iterations.csv');
lower_threshold = np.genfromtxt('lower_threshold.csv', delimiter=';', names=['x','lower']);
decentralized_lower_threshold = np.genfromtxt('decentralized_lower_threshold.csv', delimiter=';', names=['x','lower','nb_shutdowns']);

# CENTRALIZED

fig, ax = plt.subplots(figsize=(16,8))

# plot cascado-cyclic behavior
ax.axvline(x=cascado_cyclic_iterations[0], color='black', linestyle='--', linewidth=0.5, label='cascado-cyclic process iterations')
for x_ticks in cascado_cyclic_iterations:
  ax.axvline(x=x_ticks, color='black', linestyle='--', linewidth=0.5)

ax.fill_between(cascado_cyclic_state['x'], cascado_cyclic_state['y'], step="post", alpha=0.4, color='grey', label='activation of cascado-cyclic process')

# plot threshold lines
ax.axhline(y=upper_threshold, color='r', label='thresholds')
ax.step(lower_threshold['x'], lower_threshold['lower'], color='r', where='post')

ax.plot(with_cascado_cyclic['x'], with_cascado_cyclic['y'], label='centralized cascado-cyclic')
ax.plot(without_cascado_cyclic['x'], without_cascado_cyclic['y'], label='without cascado-cyclic', linestyle='--' )

plt.ylim(0, 0.53)

plt.xlim(63000, 67500)

ax.set_xlabel('time (sec)')
ax.set_ylabel('current (kA)')
#ax.grid()
ax.legend();

fig.tight_layout()
fig.savefig('line1_output.png')

plt.cla()

# DECENTRALIZED

# plot cascado-cyclic behavior
ax.fill_between(decentralized_cascado_cyclic_state['x'], decentralized_cascado_cyclic_state['y'], step="post", alpha=0.4, color='grey', label='activation of cascado-cyclic process')

# plot threshold lines
ax.axhline(y=upper_threshold, color='r', label='thresholds')
ax.step(decentralized_lower_threshold['x'], decentralized_lower_threshold['lower'], color='r', where='post')

ax.plot(decentralized_cascado_cyclic['x'], decentralized_cascado_cyclic['y'], label='decentralized cascado-cyclic')
ax.plot(without_cascado_cyclic['x'], without_cascado_cyclic['y'], label='without cascado-cyclic', linestyle='--')

plt.ylim(0, 0.53)
plt.xlim(63000, 67500)

ax.set_xlabel('time (sec)')
ax.set_ylabel('current (kA)')
#ax.grid()
ax.legend();

fig.tight_layout()
fig.savefig('decentralized_line1_output.png')

plt.cla()



# peaks duration
peaks_duration_without_cascadocyclic = np.genfromtxt('total_peaks_duration_without_cascado_cyclic.csv', delimiter=';', names=['x','y'])
peaks_duration = np.genfromtxt('total_peaks_duration.csv', delimiter=';', names=['x','y'])
decentralized_peaks_duration = np.genfromtxt('decentralized_total_peaks_duration.csv', delimiter=';', names=['x','y'])

ax.plot(peaks_duration_without_cascadocyclic['x'], peaks_duration_without_cascadocyclic['y'], label='without cascado cyclic')
ax.plot(peaks_duration['x'], peaks_duration['y'], label='with centralized cascado cyclic')
ax.plot(decentralized_peaks_duration['x'], decentralized_peaks_duration['y'], label='decentralized cascado cyclic')

plt.xlim(63000, 67500)
plt.ylim(0, 700)

ax.set_xlabel('time (sec)')
ax.set_ylabel('cumulation peaks duration (sec)')

ax.grid()
ax.legend();

fig.tight_layout()
fig.savefig('peaks_duration.png')

plt.cla()

# messages sent
messages_sent = np.genfromtxt('nb_messages_sent.csv', delimiter=';', names=['x','y'])
decentralized_messages_sent = np.genfromtxt('decentralized_nb_messages_sent.csv', delimiter=';', names=['x','y'])

ax.step(messages_sent['x'], messages_sent['y'], label = 'centralized cascado cyclic')
ax.step(decentralized_messages_sent['x'], decentralized_messages_sent['y'], label = 'decentralized cascado cyclic')

plt.xlim(63000, 67500)
plt.ylim(0,)

ax.set_xlabel('time(sec)')
ax.set_ylabel('number of messages sent')

ax.grid()
ax.legend();

fig.tight_layout()
fig.savefig('nb_messages_sent.png')

plt.cla()

#heaters status
heaters_status = []
heaters_status.append(np.genfromtxt('heaters_status.csv', delimiter=";"))
heaters_status.append(np.genfromtxt('decentralized_heaters_status.csv', delimiter=";"))
durations = []
for tab in heaters_status:
	total_duration = []
        max_duration = -1
        min_duration = sys.float_info.max
	avg_duration = 0.
	nb_duration = 0
	for i in np.arange(1, nb_heaters + 1):
 		last_status = 1;
		last_off_time = 63000;
        	duration = 0
		for event in tab:
			if event[i] != last_status:
				last_status = event[i]
				if last_status == 1:
					current_duration = event[0] - last_off_time
					duration += current_duration
                                        if current_duration > max_duration:
						max_duration = current_duration
					if current_duration < min_duration:
						min_duration = current_duration
					avg_duration += current_duration
					nb_duration += 1
				else:
					last_off_time = event[0]
		if last_status == 0:
			current_duration = 66600 - last_off_time
			duration += current_duration
			if current_duration > max_duration:
				max_duration = current_duration
			if current_duration < min_duration:
				min_duration = current_duration
			avg_duration += current_duration
			nb_duration += 1

		total_duration.append(duration)

	avg_duration /= nb_duration
	print("average duration %.3f sec" % avg_duration)
	print("max duration %.3f sec" % max_duration)
	print("min duration %.3f sec" % min_duration)
	print("total duration %.3f sec" % np.sum(total_duration))
	durations.append(total_duration)	

width = 0.4
ind_centralized = np.arange(-width/2, nb_heaters-width/2)
ind_decentralized = np.arange(width/2, nb_heaters+width/2)

ax.bar(ind_centralized, durations[0], width, label='centralized cascado-cyclic')
ax.bar(ind_decentralized, durations[1], width, label='decentralized cascado-cyclic')

ax.axhline(y=np.average(durations[0]), color='red', label='average for centralized cascado-cyclic')
ax.axhline(y=np.average(durations[1]), color='green', label='average for decentralized cascado-cyclic')

#ax.grid()
ax.legend()


ax.set_xlabel('heater ID')
ax.set_ylabel('cumulative shutdown time per heater (sec)')

fig.tight_layout()
fig.savefig('shutdown_duration.png')

#total shutdown duration
plt.cla()
ind = np.arange(2)

bottoms = [0, 0]

plt.bar(ind, [durations[0][0], durations[1][0]], width, label='heater_0')
for i in np.arange(1,nb_heaters):
        bottoms[0] += durations[0][i-1]
        bottoms[1] += durations[1][i-1]
	plt.bar(ind, [durations[0][i], durations[1][i]], width, bottom=bottoms, label='heater_'+str(i))

plt.xticks(ind, ('centralized','decentralized'))
ax.set_ylabel('overall cumulative shutdown time (sec.heater)')
#ax.legend()

fig.tight_layout()
fig.savefig('overall_shutdown_duration.png')
