#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <math.h>
#include <vector>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

/**
* In this example, we load an FMU exported with PowerFactory-FMU.
*/
double step_size = 10000000000.0;

std::string fmu_name = "proxy_fmu";

std::vector<double> Pext1 = {-1.0, -0.5, -1.0, -1.5, -2.0};
std::vector<double> Pext2 = {2.0, 0.5, 1.0, 1.5, 2.0};
std::vector<double> input_times = {0, 60, 120, 180, 240};
std::vector<double> output_times = {30, 90, 150, 210, 270};

/**
 * Actor behaviors
 */
static void waiter(){
	XBT_INFO("start simulation of power_factory");
       	for(int i = 0; i < input_times.size(); i++){
	  simgrid::s4u::this_actor::sleep_until(input_times[i]);
          simgrid::fmi::FMIPlugin::setRealInput(fmu_name, "EvtParam.Controller.Pext1", Pext1[i]);
	  simgrid::fmi::FMIPlugin::setRealInput(fmu_name, "EvtParam.Controller.Pext2", Pext2[i]); 
	}	
}

static void observer(){

        XBT_INFO("start simulation of power_factory");
        //simgrid::fmi::FMIPlugin::registerEvent(reactOnPositiveValue, testEventTime, {});
        for(int i = 0; i < output_times.size(); i++){
          simgrid::s4u::this_actor::sleep_until(output_times[i]);
          double buslv = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, "ElmTr2.Transformer.m:P:buslv");
          double bus1 = simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, "ElmLod.Generation.m:I1:bus1");
          XBT_INFO("buslv = %f , bus1 = %f", buslv, bus1);
        }
        XBT_INFO("end of simulation of power_factory");

}

int main(int argc, char *argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  simgrid::fmi::FMIPlugin::initFMIPlugin(step_size);

  e.load_platform("../../platforms/clusters_rennes.xml");
  
  simgrid::s4u::Actor::create("waiter", e.get_all_hosts()[0], waiter);
  simgrid::s4u::Actor::create("observer", e.get_all_hosts()[0], observer);

  std::string fmu_uri = (argc>=2)? argv[1] : "file://power_factory";
 
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name, false);

  simgrid::fmi::FMIPlugin::readyForSimulation();
  e.run();

  return 0;
}
