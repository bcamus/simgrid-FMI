#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <math.h>


XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

/**
* In this example, we consider a simple FMU with an output variable x=sin(time).
* We test that we can properly detect and trigger event when x cross the x = 0 threshold.
*/

std::string fmu_name = "proxy_fmu";
const double step_size = 0.001;
double error = 0.0001;
int expected_detections = 100;
double max_sim_time = (expected_detections-1) * M_PI + 1;

/**
 * Event detection functions
 */

static bool reactOnNegativeValue(std::vector<std::string>){
	return simgrid::fmi::FMIPlugin::getRealOutput(fmu_name, "x") < 0;
}

static bool reactOnPositiveValue(std::vector<std::string>){
	return simgrid::fmi::FMIPlugin::getIntegerOutput(fmu_name,"positive") == 1;
}

/**
 * Event handlers
 */

int nb_event = 0;

static void testEventTime(std::vector<std::string>){

	double actual_time = simgrid::s4u::Engine::get_clock();
	double expected_time = nb_event * M_PI;
	nb_event++;

        assert(actual_time <= expected_time + step_size + error && actual_time >= expected_time - error); 
	XBT_INFO("correct zero crossing detection of sin(time) at time %f",actual_time);

	if(nb_event % 2 !=0){
		simgrid::fmi::FMIPlugin::registerEvent(reactOnNegativeValue,testEventTime,{});
	}else{
		simgrid::fmi::FMIPlugin::registerEvent(reactOnPositiveValue,testEventTime,{});
	}
}


/**
 * Actor behaviors
 */
static void waiter(){
	XBT_INFO("start simulation of x=sin(time)");
	simgrid::fmi::FMIPlugin::registerEvent(reactOnPositiveValue, testEventTime, {});
	simgrid::s4u::this_actor::sleep_until(max_sim_time);
	XBT_INFO("end of simulation of sin(time), zero crossings have been correctly detected");
	
	if(nb_event != expected_detections){
		XBT_ERROR("error! %i zero crossings have been detected whereas %i detections were expected", nb_event, expected_detections);
	}
}

int main(int argc, char *argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  simgrid::fmi::FMIPlugin::initFMIPlugin(step_size);

  e.load_platform("../../platforms/clusters_rennes.xml");
  
  simgrid::s4u::Actor::create("waiter", e.get_all_hosts()[0], waiter);

  std::string fmu_uri = (argc>=2)? argv[1] : "file://proxy_fmu";
 
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name);

  simgrid::fmi::FMIPlugin::readyForSimulation();
  e.run();

  return 0;
}
