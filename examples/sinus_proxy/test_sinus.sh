#!/bin/bash

cd examples/sinus

# we run the FMU simulation of a simple sinus with SimGrid
sinus="$(find `pwd` -name sinus_fmu)"
./s4u-sinus "file://$sinus"

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the SimGrid simulation did not end properly"
        exit 1
fi
