#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

// SYSTEM'S STATE

const double max_sim_time = 15;
const double sampling_time = 0.01;

// UTILITY

std::ofstream out;

static void checkResults(){
        double t = simgrid::s4u::Engine::get_clock();
        double i = simgrid::fmi::FMIPlugin::getRealOutput("RLC","i");
        double uc = simgrid::fmi::FMIPlugin::getRealOutput("RLC","Uc");
	double ul = simgrid::fmi::FMIPlugin::getRealOutput("RLC","Ul");
	double ur = simgrid::fmi::FMIPlugin::getRealOutput("RLC","Ur");
        out << t << ";" << uc << ";" << i << ";" << ul << ";" << ur << "\n";
}

static void sampler(std::vector<std::string>){
	XBT_INFO("perform simulation of RLC system");
	checkResults();
	while(simgrid::s4u::Engine::get_clock() < max_sim_time){
		simgrid::s4u::this_actor::sleep_for(sampling_time);
		checkResults();
	}
	out.close();
	XBT_INFO("co-simulation of RLC system done ! see you !");
}

// MAIN

int main(int argc, char *argv[])
{

  out.open("output.csv", std::ios::out);
  out << "\"time\",\"Uc\",\"i\",\"Ul\",\"Ur\"\n";

  // SIMGRID INIT
  simgrid::s4u::Engine e(&argc, argv);


  const double intstepsize = 1;
  simgrid::fmi::FMIPlugin::initFMIPlugin(intstepsize);

  e.load_platform("../../platforms/clusters_rennes.xml");

  std::vector<std::string> args;

  // ADDING FMUs

  std::string fmu_uri = (argc >= 2)? argv[1] : "RLC_FMU/RLC.fmu";

  std::string fmu_name = "RLC";
  simgrid::fmi::FMIPlugin::addFMUCS(fmu_uri, fmu_name);

  simgrid::fmi::FMIPlugin::readyForSimulation();

  // CREATING SIMGRID ACTORS

  simgrid::s4u::Actor::create("sampling_actor", simgrid::s4u::Host::by_name("c-0.rennes"), sampler, args);

  e.run();
}
