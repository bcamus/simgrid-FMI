//#include "src/surf/surf_interface.hpp"
#include "simgrid-fmi.hpp"
#include <vector>
#include <unordered_map>
#include <simgrid/simix.hpp>

XBT_LOG_NEW_DEFAULT_SUBCATEGORY(surf_fmi, surf, "Logging specific to the SURF FMI plugin");


namespace simgrid{
namespace fmi{

MasterFMI* FMIPlugin::master;


/**
 * FMIPlugin
 *
 */

simgrid::xbt::signal<void()> FMIPlugin::on_state_change;

FMIPlugin::FMIPlugin(){
}

FMIPlugin::~FMIPlugin(){
}

void FMIPlugin::addFMUCS(std::string fmu_uri, std::string fmu_name, bool iterateAfterInput){
	master->addFMUCS(fmu_uri, fmu_name, iterateAfterInput);
}

void FMIPlugin::connectFMU(std::string out_fmu_name,std::string output_port,std::string in_fmu_name,std::string input_port){
	master->connectFMU(out_fmu_name,output_port,in_fmu_name,input_port);
}

void FMIPlugin::connectRealFMUToSimgrid(double (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){
	master->connectRealFMUToSimgrid(generateInput,params,fmu_name,input_name);
}

void FMIPlugin::connectIntegerFMUToSimgrid(int (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){
	master->connectIntegerFMUToSimgrid(generateInput,params,fmu_name,input_name);
}

void FMIPlugin::connectBooleanFMUToSimgrid(bool (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){
	master->connectBooleanFMUToSimgrid(generateInput,params,fmu_name,input_name);
}

void FMIPlugin::connectStringFMUToSimgrid(std::string (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){
	master->connectStringFMUToSimgrid(generateInput,params,fmu_name,input_name);
}


void FMIPlugin::initFMIPlugin(double communication_step){
	if(master == 0){
		master = new MasterFMI(communication_step);
	}
}

void FMIPlugin::readyForSimulation(){
	master->initCouplings();
	all_existing_models.push_back(master);
}

double FMIPlugin::getRealOutput(std::string fmi_name, std::string output_name){
	return master->getRealOutput(fmi_name, output_name, true);
}

bool FMIPlugin::getBooleanOutput(std::string fmi_name, std::string output_name){
	return master->getBooleanOutput(fmi_name, output_name, true);
}

int FMIPlugin::getIntegerOutput(std::string fmi_name, std::string output_name){
	return master->getIntegerOutput(fmi_name, output_name, true);
}

std::string FMIPlugin::getStringOutput(std::string fmi_name, std::string output_name){
	return master->getStringOutput(fmi_name, output_name, true);
}

void FMIPlugin::setRealInput(std::string fmi_name, std::string input_name, double value){
	simgrid::kernel::actor::simcall([fmi_name, input_name,value]() {
		master->setRealInput(fmi_name, input_name, value, true);
	});
}

void FMIPlugin::setBooleanInput(std::string fmi_name, std::string input_name, bool value){
	simgrid::kernel::actor::simcall([fmi_name, input_name,value]() {
		master->setBooleanInput(fmi_name, input_name, value, true);
	});
}

void FMIPlugin::setIntegerInput(std::string fmi_name, std::string input_name, int value){
	simgrid::kernel::actor::simcall([fmi_name, input_name,value]() {
		master->setIntegerInput(fmi_name, input_name, value, true);
	});
}

void FMIPlugin::setStringInput(std::string fmi_name, std::string input_name, std::string value){
	simgrid::kernel::actor::simcall([fmi_name, input_name,value]() {
		master->setStringInput(fmi_name, input_name, value, true);
	});

}

void FMIPlugin::registerEvent(bool (*condition)(std::vector<std::string>), void (*handleEvent)(std::vector<std::string>), std::vector<std::string> params){
	simgrid::kernel::actor::simcall([condition,handleEvent,params]() {
		master->registerEvent(condition,handleEvent,params);
	});
}

void FMIPlugin::deleteEvents(){
	master->deleteEvents();
}

void FMIPlugin::configureOutputLog(std::string output_file_path, std::vector<port> ports_to_monitor){
	master->configureOutputLog(output_file_path,ports_to_monitor);
}



/**
 * MasterFMI
 */

MasterFMI::MasterFMI(const double stepSize)
: Model(simgrid::kernel::resource::Model::UpdateAlgo::LAZY){

	commStep = stepSize;
	nextEvent = -1;
	current_time = 0;
	firstEvent = true;
	externalCoupling = false;
	ready_for_simulation = false;
}


MasterFMI::~MasterFMI() {
	output.close();
}


void MasterFMI::addFMUCS(std::string fmu_uri, std::string fmu_name, bool iterateAfterInput){

        const double start_time = SIMIX_get_clock();

        fmi4cpp::fmi2::fmu fmu(fmu_uri);
        auto cs_fmu = fmu.as_cs_fmu();
        fmus[fmu_name] = cs_fmu->new_instance();

	XBT_DEBUG("FMU-CS loaded");

	fmus[fmu_name]->setup_experiment(start_time);
        fmus[fmu_name]->enter_initialization_mode();
        fmus[fmu_name]->exit_initialization_mode();

        XBT_DEBUG("FMU-CS initialized");
}


void MasterFMI::connectFMU(std::string out_fmu_name,std::string output_port,std::string in_fmu_name,std::string input_port){

	checkNotReadyForSimulation();
	checkPortValidity(out_fmu_name,output_port,fmi4cpp::fmi2::UNKNOWN_TYPE,false);
	std::string out_type = fmus[out_fmu_name]->get_model_description()->get_variable_by_name(output_port).type_name();
	checkPortValidity(in_fmu_name,input_port,out_type,true);

	port out;
	port in;
	out.fmu = out_fmu_name;
	out.name = output_port;
	in.fmu = in_fmu_name;
	in.name = input_port;
	in_coupled_input.push_back(in);
	couplings[in]=out;
}

void MasterFMI::connectRealFMUToSimgrid(double (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){

	checkNotReadyForSimulation();
	checkPortValidity(fmu_name,input_name,fmi4cpp::fmi2::REAL_TYPE,true);

	externalCoupling = true;

	real_simgrid_fmu_connection connection;
	port in;
	in.fmu = fmu_name;
	in.name = input_name;
	connection.in = in;
	connection.generateInput = generateInput;
	connection.params = params;

	real_ext_couplings.push_back(connection);
	ext_coupled_input.push_back(in);
}

void MasterFMI::connectIntegerFMUToSimgrid(int (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){

	checkNotReadyForSimulation();
	checkPortValidity(fmu_name,input_name,fmi4cpp::fmi2::INTEGER_TYPE,true);

	externalCoupling = true;

	integer_simgrid_fmu_connection connection;
	port in;
	in.fmu = fmu_name;
	in.name = input_name;
	connection.in = in;
	connection.generateInput = generateInput;
	connection.params = params;

	integer_ext_couplings.push_back(connection);
	ext_coupled_input.push_back(in);
}

void MasterFMI::connectBooleanFMUToSimgrid(bool (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){

	checkNotReadyForSimulation();
	checkPortValidity(fmu_name,input_name,fmi4cpp::fmi2::BOOLEAN_TYPE,true);

	externalCoupling = true;

	boolean_simgrid_fmu_connection connection;
	port in;
	in.fmu = fmu_name;
	in.name = input_name;
	connection.in = in;
	connection.generateInput = generateInput;
	connection.params = params;

	boolean_ext_couplings.push_back(connection);
	ext_coupled_input.push_back(in);
}

void MasterFMI::connectStringFMUToSimgrid(std::string (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name){

	checkNotReadyForSimulation();
	checkPortValidity(fmu_name,input_name,fmi4cpp::fmi2::STRING_TYPE,true);

	externalCoupling = true;

	string_simgrid_fmu_connection connection;
	port in;
	in.fmu = fmu_name;
	in.name = input_name;
	connection.in = in;
	connection.generateInput = generateInput;
	connection.params = params;

	string_ext_couplings.push_back(connection);
	ext_coupled_input.push_back(in);
}



double MasterFMI::getRealOutput(std::string fmi_name, std::string output_name, bool checkPort){

	if(checkPort)
		checkPortValidity(fmi_name,output_name,fmi4cpp::fmi2::REAL_TYPE,false);

	double out;

        auto fmu_md = fmus[fmi_name]->get_model_description();
        auto var = fmu_md->get_variable_by_name(output_name).as_real();
        if (!var.read(*(fmus[fmi_name]), out))
		xbt_die("FMI %s failed to return the value of variable %s",fmi_name.c_str(),output_name.c_str());

	return out;
}

bool MasterFMI::getBooleanOutput(std::string fmi_name, std::string output_name, bool checkPort){

	if(checkPort)
		checkPortValidity(fmi_name,output_name,fmi4cpp::fmi2::BOOLEAN_TYPE,false);

	bool out;

        auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(output_name).as_boolean();

        if (!var.read(*(fmus[fmi_name]), out))
		xbt_die("FMI %s failed to return the value of variable %s",fmi_name.c_str(),output_name.c_str());

	return out;
}

int MasterFMI::getIntegerOutput(std::string fmi_name, std::string output_name, bool checkPort){

	if(checkPort)
		checkPortValidity(fmi_name,output_name,fmi4cpp::fmi2::INTEGER_TYPE,false);

        int out;

	auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(output_name).as_integer();

        if (!var.read(*(fmus[fmi_name]), out))
		xbt_die("FMI %s failed to return the value of variable %s",fmi_name.c_str(),output_name.c_str());

	return out;
}

std::string MasterFMI::getStringOutput(std::string fmi_name, std::string output_name, bool checkPort){

	if(checkPort)
		checkPortValidity(fmi_name,output_name,fmi4cpp::fmi2::STRING_TYPE,false);

	std::string out;

	auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(output_name).as_string();

        if (!var.read(*(fmus[fmi_name]), out))
		xbt_die("FMI %s failed to return the value of variable %s",fmi_name.c_str(),output_name.c_str());

	return out;
}

void MasterFMI::setRealInput(std::string fmi_name, std::string input_name, double value, bool simgrid_input){

	if(simgrid_input)
		checkPortValidity(fmi_name,input_name,fmi4cpp::fmi2::REAL_TYPE,simgrid_input);

        auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(input_name).as_real();

        if (!var.write(*(fmus[fmi_name]), value))
		xbt_die("FMU %s failed to set its port %s to value %f",fmi_name.c_str(),input_name.c_str(),value);

	if(iterate_input[fmi_name]){
		if(!fmus[fmi_name]->step(0.))
			xbt_die("FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set iterateAfterInput=false when adding the FMU CS).",fmi_name.c_str());
	}

	if(simgrid_input && ready_for_simulation){
		solveCouplings(false);
		manageEventNotification();
	}
}

void MasterFMI::setBooleanInput(std::string fmi_name, std::string input_name, bool value, bool simgrid_input){

	if(simgrid_input)
		checkPortValidity(fmi_name,input_name,fmi4cpp::fmi2::BOOLEAN_TYPE,simgrid_input);

        auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(input_name).as_boolean();

        if (!var.write(*(fmus[fmi_name]), value))
		xbt_die("FMU %s failed to set its port %s to value %f",fmi_name.c_str(),input_name.c_str(),value);

	if(iterate_input[fmi_name]){
		if(!fmus[fmi_name]->step(0.))
			xbt_die("FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set iterateAfterInput=false when adding the FMU CS).",fmi_name.c_str());
	}

	if(simgrid_input && ready_for_simulation){
		solveCouplings(false);
		manageEventNotification();
	}
}

void MasterFMI::setIntegerInput(std::string fmi_name, std::string input_name, int value, bool simgrid_input){

	if(simgrid_input)
		checkPortValidity(fmi_name,input_name,fmi4cpp::fmi2::INTEGER_TYPE,simgrid_input);

        auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(input_name).as_integer();

        if (!var.write(*(fmus[fmi_name]), value))
		xbt_die("FMU %s failed to set its port %s to value %f",fmi_name.c_str(),input_name.c_str(),value);

	if(iterate_input[fmi_name]){
		if(!fmus[fmi_name]->step(0.))
			xbt_die("FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set iterateAfterInput=false when adding the FMU CS).",fmi_name.c_str());
	}

	if(simgrid_input && ready_for_simulation){
		solveCouplings(false);
		manageEventNotification();
	}
}

void MasterFMI::setStringInput(std::string fmi_name, std::string input_name, std::string value, bool simgrid_input){

	if(simgrid_input)
		checkPortValidity(fmi_name,input_name, fmi4cpp::fmi2::STRING_TYPE, simgrid_input);

        auto fmu_md = fmus[fmi_name]->get_model_description();
	auto var = fmu_md->get_variable_by_name(input_name).as_string();

        if (!var.write(*(fmus[fmi_name]), value))
		xbt_die("FMU %s failed to set its port %s to value %f",fmi_name.c_str(),input_name.c_str(),value);

	if(iterate_input[fmi_name]){
		if(!fmus[fmi_name]->step(0.))
			xbt_die("FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set iterateAfterInput=false when adding the FMU CS).",fmi_name.c_str());
	}

	if(simgrid_input && ready_for_simulation){
		solveCouplings(false);
		manageEventNotification();
	}
}

void MasterFMI::solveCouplings(bool firstIteration){

	bool change = true;
	int i = 0;
	while(change){
		change = false;
		for(port in : in_coupled_input){
			change = (solveCoupling(in, couplings[in],!firstIteration) || change);
		}
		if(firstIteration)
			firstIteration = false;
		i++;
	}

	FMIPlugin::on_state_change();
	logOutput();
}

bool MasterFMI::solveCoupling(port in, port out, bool checkChange){

	bool change = false;

	auto var = fmus[out.fmu]->get_model_description()->get_variable_by_name(out.name);
        if(var.is_real()){
		double r_out = getRealOutput(out.fmu, out.name);
		if( !checkChange || r_out !=  last_real_outputs[out]){
			setRealInput(in.fmu, in.name, r_out,false);
			last_real_outputs[out] = r_out;
			change = true;
		}

	}else if(var.is_integer()){
		int i_out = getIntegerOutput(out.fmu, out.name);
		if( !checkChange || i_out != last_int_outputs[out]){
			setIntegerInput(in.fmu, in.name, i_out,false);
			last_int_outputs[out] = i_out;
			change = true;
		}
	}

	else if(var.is_boolean()){
		bool b_out = getBooleanOutput(out.fmu, out.name);
		if( !checkChange || b_out != last_bool_outputs[out]){
			setBooleanInput(in.fmu, in.name, b_out,false);
			last_bool_outputs[out] = b_out;
			change = true;
		}

	}else if(var.is_string()){
		std::string s_out = getStringOutput(out.fmu, out.name);
		if( !checkChange || s_out != last_string_outputs[out]){
			setStringInput(in.fmu, in.name, s_out,false);
			last_string_outputs[out] = s_out;
			change = true;
		}
	}

	return change;
}

void MasterFMI::solveExternalCoupling(){

	for(real_simgrid_fmu_connection coupling : real_ext_couplings){
		double input = coupling.generateInput(coupling.params);
		setRealInput(coupling.in.fmu, coupling.in.name, input,false);
	}

	for(integer_simgrid_fmu_connection coupling : integer_ext_couplings){
		int input = coupling.generateInput(coupling.params);
		setIntegerInput(coupling.in.fmu, coupling.in.name, input,false);
	}

	for(boolean_simgrid_fmu_connection coupling : boolean_ext_couplings){
		bool input = coupling.generateInput(coupling.params);
		setBooleanInput(coupling.in.fmu, coupling.in.name, input,false);
	}

	for(string_simgrid_fmu_connection coupling : string_ext_couplings){
		std::string input = coupling.generateInput(coupling.params);
		setStringInput(coupling.in.fmu, coupling.in.name, input,false);
	}
}


void MasterFMI::update_actions_state(double now, double delta){

	XBT_DEBUG("updating the FMUs at time = %f, delta = %f",now,delta);
	
	while(current_time < now){
		double dt = std::min(commStep, now - current_time);
		XBT_DEBUG("current_time = %f perform doStep of %f ",current_time, dt);
		for(auto& it : fmus){
			if(!it.second->step(dt))
				xbt_die("FMU %s failed to go from time %f to time %f during the co-simulation",*it.first.c_str(),current_time,(current_time+dt));
		}
		current_time += dt;
		if(current_time != now){
			solveCouplings(true);
		}
	}

	solveExternalCoupling();
	solveCouplings(true);
	manageEventNotification();	
}

void MasterFMI::initCouplings(){
	ready_for_simulation = true;
	solveExternalCoupling();
	solveCouplings(true);
	manageEventNotification();
}

double MasterFMI::next_occurring_event(double now){

	// we first solve the external couplings if some of the model states have been changed by the actors
  if(externalCoupling){
    solveExternalCoupling();
    solveCouplings(true); // We may need to re-propagate the results, e.g. if actors generated some instantaneous events
	}

	if(firstEvent || checkEventOccurence()){
		firstEvent = false;
		return 0;
	}else if(event_handlers.size()==0){
		return -1;
	}else{
		return commStep;
	}
}


void MasterFMI::registerEvent(bool (*condition)(std::vector<std::string>),
	void (*handleEvent)(std::vector<std::string>),
	std::vector<std::string> handlerParam){

	if(condition(handlerParam)){
		handleEvent(handlerParam);
	}else{
		event_handlers.push_back(handleEvent);
		event_conditions.push_back(condition);
		event_params.push_back(handlerParam);
	}
}

bool MasterFMI::checkEventOccurence(){
	for(int i = 0;i<event_handlers.size();i++){
        	if((*event_conditions[i])(event_params[i]))
			return true;	
	}
	return false;
}

void MasterFMI::manageEventNotification(){
	int size = event_handlers.size();
	for(int i = 0;i<event_handlers.size();i++){

		bool isEvent = (*event_conditions[i])(event_params[i]);
		if(isEvent){

			event_conditions.erase(event_conditions.begin()+i);
			void (*handleEvent)(std::vector<std::string>) = event_handlers[i];
			event_handlers.erase(event_handlers.begin()+i);
			std::vector<std::string> handlerParam = event_params[i];
			event_params.erase(event_params.begin()+i);
			i--;

			(*handleEvent)(handlerParam);
		}
	}
}

void MasterFMI::deleteEvents(){
	event_handlers.clear();
	event_conditions.clear();
	event_params.clear();
}

bool MasterFMI::isInputCoupled(std::string fmu, std::string input_name){
	port input;
	input.fmu = fmu;
	input.name = input_name;
	return std::find(in_coupled_input.begin(), in_coupled_input.end(), input) != in_coupled_input.end()
			|| std::find(ext_coupled_input.begin(), ext_coupled_input.end(), input) != ext_coupled_input.end();
}

void MasterFMI::checkPortValidity(std::string fmu_name, std::string port_name, std::string type, bool check_already_coupled){

	if(fmus.find(fmu_name)==fmus.end())
		xbt_die("unknown FMU %s",fmu_name.c_str());

	std::string var_type = fmus[fmu_name]->get_model_description()->get_variable_by_name(port_name).type_name();

	if(type != fmi4cpp::fmi2::UNKNOWN_TYPE && var_type != type)
		xbt_die("wrong type compatibility for port %s of FMU %s.",port_name.c_str(),fmu_name.c_str());

	if(check_already_coupled && isInputCoupled(fmu_name,port_name))
		xbt_die("port %s of FMU %s is already coupled to a model",port_name.c_str(),fmu_name.c_str());
}

void MasterFMI::checkNotReadyForSimulation(){
	if(ready_for_simulation)
		xbt_die("you can not modify the FMI model after calling simgrid::fmi::FMIPlugin::readyForSimulation().");
}


void MasterFMI::configureOutputLog(std::string output_file_path, std::vector<port> ports_to_monitor){
	output.open(output_file_path, std::ios::out);
	monitored_ports = ports_to_monitor;
	for(port p : monitored_ports){
		checkPortValidity(p.fmu,p.name, fmi4cpp::fmi2::UNKNOWN_TYPE,false);
	}
}

void MasterFMI::logOutput(){
	output << current_time;

	for(port p : monitored_ports){
		auto var = fmus[p.fmu]->get_model_description()->get_variable_by_name(p.name);
		if(var.is_real()){
			output << ";" << getRealOutput(p.fmu, p.name);
		}else if(var.is_integer()){
			output << ";" << getIntegerOutput(p.fmu, p.name);
		}else if(var.is_boolean()){
			output << ";" << getBooleanOutput(p.fmu, p.name);
		}else if(var.is_string()){
			output << ";" << getStringOutput(p.fmu, p.name);
		}
	}

	output << "\n";
}


}
}
