#define LOCAL_FMU_MANAGER
#include "FMUCoSimulation_v1.h"
#include "FMUCoSimulation_v2.h"
#include "ModelManager.h"
#include <fmiModelTypes.h>
#include <FMIVariableType.h>

#include "proxyFmuTypes.h"

#if defined (WIN32)
  #include <winsock2.h>
  typedef int socklen_t;
#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #include <unistd.h>

  #define INVALID_SOCKET -1
  #define SOCKET_ERROR -1
  #define closesocket(s) close(s)

  typedef int SOCKET;
  typedef struct sockaddr_in SOCKADDR_IN;
  typedef struct sockaddr SOCKADDR;
#endif

#include <stdio.h>
#include <stdlib.h>
#define PORT 23

SOCKET sock;
FMUCoSimulationBase *model;

std::string instanceName;
std::string fmuLocation;

void loadFMU(){
  FMUType fmuType = invalid;
  fprintf(stderr,"loading FMU %s at path %s", instanceName.c_str(), fmuLocation.c_str());
  ModelManager::LoadFMUStatus loadStatus = ModelManager::loadFMU(instanceName, fmuLocation, false, fmuType);
  if ((ModelManager::success != loadStatus) && (ModelManager::duplicate != loadStatus))
	  //perror("modelmanager load fmu");
	  fprintf(stderr, "modelmanager load fmu %s at location %s\n", instanceName.c_str(), fmuLocation.c_str());

  if(fmi_1_0_cs == fmuType){
    model = new fmi_1_0::FMUCoSimulation(instanceName, true, 1e-4);
  }else if( (fmi_2_0_cs == fmuType) || (fmi_2_0_me_and_cs == fmuType)){
    model = new fmi_2_0::FMUCoSimulation(instanceName, true, 1e-4);
  }

  fprintf(stderr, "done loading FMU");
}

void mainLoop(){

  callType call;
  fmiStatus ok = fmiOK;
  fmiStatus fatal = fmiFatal;
  fmiStatus out_status;
  fmiValueReference ref;  
  //printf("start simulation");
  do{
	 // fprintf(stderr, "reading call (size = %lu)", sizeof(call));
    if(recv(sock, reinterpret_cast<char *>(&call), sizeof(call), MSG_WAITALL) != SOCKET_ERROR){
      switch(call){        
        // INSTANTIATE FMU
        case instantiate : 
		  //fprintf(stderr, "[LOCAL] instantiating the FMU\n");
          model->instantiate(instanceName, 0.0, fmiFalse, fmiFalse);
		  //fprintf(stderr, "[LOCAL] instantiation done\n");
		  send(sock, reinterpret_cast<char *>(&ok), sizeof(ok), 0);
          break;

        case initialize :
          initializeParam init_param;
		  if (recv(sock, reinterpret_cast<char *>(&init_param), sizeof(init_param), MSG_WAITALL) != SOCKET_ERROR){
			 //fprintf(stderr, "[LOCAL] initializing the FMU\n");
			 out_status = model->initialize(init_param.tStart, init_param.stopTimeDefined, init_param.tStop);
			 send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
			 //fprintf(stderr, "[LOCAL] initialization done\n");
          }else{
			  send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("initialize_param\n");
          }
          break;

        case terminateFMU :
			send(sock, reinterpret_cast<char *>(&ok), sizeof(ok), 0);
          break;

        case doStep :
             
          doStepParam doStep_param;
		  if (recv(sock, reinterpret_cast<char *>(&doStep_param), sizeof(doStep_param), MSG_WAITALL) != SOCKET_ERROR){
			//fprintf(stderr, "[LOCAL] doing a doStep of %f from %f\n", doStep_param.commStep, doStep_param.currentTime);
			out_status = model->doStep(doStep_param.currentTime, doStep_param.commStep, doStep_param.newStep);
			//fprintf(stderr, "[LOCAL] doStep done\n");
            if(out_status != fmiOK)
	         fprintf(stderr, "[LOCAL] fail to do doStep from %f to %f (new step = %i)\n", doStep_param.currentTime, doStep_param.commStep, doStep_param.newStep);
	   
			send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
          }else{
			  send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("doStep_param");
          }
          break;

        case getReal :
			if (recv(sock, reinterpret_cast<char *>(&ref), sizeof(ref), MSG_WAITALL) != SOCKET_ERROR){
            //fprintf(stderr, "[LOCAL] getting real for variable number %u\n", ref);
            getRealReturn real_ret;
            real_ret.status = model->getValue(&ref, &(real_ret.value), 1);
			//fprintf(stderr, "[LOCAL] get value");
            //fprintf(stderr, "[LOCAL] sending status and real value %f (size %lu + %lu = %lu)\n", real_ret.value, sizeof(real_ret.status), sizeof(real_ret.value), sizeof(real_ret));
			send(sock, reinterpret_cast<char *>(&real_ret), sizeof(real_ret), 0);
          }else{
			send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("getReal_param");
          }
          break;
        
        case getInteger :
			if (recv(sock, reinterpret_cast<char *>(&ref), sizeof(ref), MSG_WAITALL) != SOCKET_ERROR){
            //fprintf(stderr, "[LOCAL] getting integer for variable number %u \n", ref);
            getIntegerReturn integer_ret;
            integer_ret.status = model->getValue(&ref, &(integer_ret.value), 1);
            //fprintf(stderr, "[LOCAL] sending status and integer value (size %lu) \n", sizeof(integer_ret));
			send(sock, reinterpret_cast<char *>(&integer_ret), sizeof(integer_ret), 0);
          }else{
				send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("getInteger_param");
          }
          break;

        case getBoolean :
		  if (recv(sock, reinterpret_cast<char *>(&ref), sizeof(ref), MSG_WAITALL) != SOCKET_ERROR){
            getBooleanReturn bool_ret;
            bool_ret.status = model->getValue(&ref, &(bool_ret.value), 1);
			send(sock, reinterpret_cast<char *>(&bool_ret), sizeof(bool_ret), 0);
          }else{
				send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("getBoolean_param");
          }
          break;

        case getString :
		  if (recv(sock, reinterpret_cast<char *>(&ref), sizeof(ref), MSG_WAITALL) != SOCKET_ERROR){
            getStringReturn string_ret;
            std::string value = "";
            string_ret.status = model->getValue(&ref, &value, 1);
            string_ret.value = value.c_str();
			send(sock, reinterpret_cast<char *>(&string_ret), sizeof(string_ret), 0);
          }else{
			send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("getString_param");
          }
          break;

        case setReal :
          setRealParam set_real_param;
		  if (recv(sock, reinterpret_cast<char *>(&set_real_param), sizeof(set_real_param), MSG_WAITALL) != SOCKET_ERROR){
			//fprintf(stderr, "setting real variable %u to %f\n", set_real_param.ref, set_real_param.value);
			out_status = model->setValue(&set_real_param.ref, &set_real_param.value, 1);
			//fprintf(stderr, "done setting real variable %u\n", set_real_param.ref);
			send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
          }else{
		    send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("setReal_param");
          }
          break;

        case setInteger :
          setIntegerParam set_int_param;
		  if (recv(sock, reinterpret_cast<char *>(&set_int_param), sizeof(set_int_param), MSG_WAITALL) != SOCKET_ERROR){
            out_status = model->setValue(&set_int_param.ref, &set_int_param.value, 1);
			send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
          }else{
            send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("setInteger_param");
          }
          break;

        case setBoolean :
          setBooleanParam set_bool_param;
		  if (recv(sock, reinterpret_cast<char *>(&set_bool_param), sizeof(set_bool_param), MSG_WAITALL) != SOCKET_ERROR){
            out_status = model->setValue(&set_bool_param.ref, &set_bool_param.value, 1);
			send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
          }else{
			send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("setInteger_param");
          }
          break;

        case setString :
          setStringParam set_string_param;
		  if (recv(sock, reinterpret_cast<char *>(&set_string_param), sizeof(set_string_param), MSG_WAITALL) != SOCKET_ERROR){
            std::string str_value = set_string_param.value;
            out_status = model->setValue(&set_string_param.ref, &str_value, 1);
			send(sock, reinterpret_cast<char *>(&out_status), sizeof(out_status), 0);
          }else{
			send(sock, reinterpret_cast<char *>(&fatal), sizeof(fatal), 0);
            perror("setInteger_param");
          }
          break;

        default : 
          perror("WARNING unknown call type !");
		  send(sock, reinterpret_cast<char *>(&ok), sizeof(ok), 0);
          break;
      }
    }else{
      fprintf(stderr, "[LOCAL MANAGER] callType recv\n");
	  model->terminate();
	  closesocket(sock);
	  return;
    } 

  }while(call != terminateFMU);
  printf("local fmu manager stop the execution of the FMU");
  closesocket(sock);
}


int main(int argc, char *argv[]){
  
  #if defined (WIN32)
    WSADATA WSAData;
    int erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
  #else
    int erreur = 0;
  #endif

  SOCKADDR_IN sin;

  instanceName = argv[1];
  fmuLocation = argv[2];

  if(!erreur){
    sock = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);

    if(connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR){
      loadFMU(); 
      mainLoop();
    }else{
      perror("client connect");
    }
  }  
}

