#define MODEL_IDENTIFIER proxy_fmu
#include "fmiFunctions.h"
#include "proxyFmuTypes.h"

#if defined (WIN32)
  #include <winsock2.h>
  typedef int socklen_t;
#elif defined (linux)
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #include <unistd.h>

  #define INVALID_SOCKET -1
  #define SOCKET_ERROR -1
  #define closesocket(s) close(s)

  typedef int SOCKET;
  typedef struct sockaddr_in SOCKADDR_IN;
  typedef struct sockaddr SOCKADDR;
#endif

#include <stdio.h>
#include <stdlib.h>
#define PORT 23

/****************************************************
Common Functions
****************************************************/

typedef struct{
  SOCKET socketID;
} slave;

slave fmu;

void sendParamsAndGetStatus(callType type, const void* params, size_t param_len, void *output, size_t output_size){
  
  //printf("triggering call type %d in the FMU\n", type);
  send(fmu.socketID, &type, sizeof(type),0);
  
  //printf("sending the parameters of size %zu\n", param_len);
  if(param_len > 0)
    send(fmu.socketID, params, param_len, 0);
  
  if(recv(fmu.socketID, output, output_size, MSG_WAITALL) == SOCKET_ERROR)
    perror("can not receive status");
}

/* Inquire version numbers of header files */
const char* fmiGetTypesPlatform(){
  return fmiPlatform; 
}
   
const char* fmiGetVersion(){
 return "1.0";
}

fmiStatus fmiSetDebugLogging(fmiComponent c, fmiBoolean loggingOn){
  return fmiOK;
}

/* Data Exchange Functions*/
fmiStatus fmiGetReal(fmiComponent c, const fmiValueReference vr[], size_t nvr, fmiReal value[]){
  
  getRealReturn ret;  
  sendParamsAndGetStatus(getReal, vr, sizeof(fmiValueReference), &ret, sizeof(ret));
  value[0] = ret.value;

  return ret.status;
}
   
fmiStatus fmiGetInteger(fmiComponent c, const fmiValueReference vr[], size_t nvr, fmiInteger value[]){
   
  getIntegerReturn ret;
  sendParamsAndGetStatus(getInteger, vr, sizeof(fmiValueReference), &ret, sizeof(ret));
  value[0] = ret.value;

  return ret.status;
}

fmiStatus fmiGetBoolean(fmiComponent c, const fmiValueReference vr[], size_t nvr, fmiBoolean value[]){
  
  getBooleanReturn ret;
  sendParamsAndGetStatus(getBoolean, vr, sizeof(fmiValueReference), &ret, sizeof(ret));
  value[0] = ret.value;

  return ret.status;
}
   
fmiStatus fmiGetString(fmiComponent c, const fmiValueReference vr[], size_t nvr, fmiString  value[]){

  getStringReturn ret;
  sendParamsAndGetStatus(getString, vr, sizeof(fmiValueReference), &ret, sizeof(ret));
  value[0] = ret.value;

  return ret.status; 
}

fmiStatus fmiSetReal(fmiComponent c, const fmiValueReference vr[], size_t nvr, const fmiReal value[]){
  
  setRealParam params = {vr[0], value[0]};
  fmiStatus status;
  sendParamsAndGetStatus(setReal, &params, sizeof(params), &status, sizeof(status));

  return status;
}
   
fmiStatus fmiSetInteger(fmiComponent c, const fmiValueReference vr[], size_t nvr, const fmiInteger value[]){
  setIntegerParam params = {vr[0], value[0]};
  fmiStatus status;
  sendParamsAndGetStatus(setInteger, &params, sizeof(params), &status, sizeof(status));

  return status;
}
   
fmiStatus fmiSetBoolean(fmiComponent c, const fmiValueReference vr[], size_t nvr, const fmiBoolean value[]){
  setBooleanParam params = {vr[0], value[0]};
  fmiStatus status;
  sendParamsAndGetStatus(setBoolean, &params, sizeof(params), &status, sizeof(status));

  return status;
}
   
fmiStatus fmiSetString(fmiComponent c, const fmiValueReference vr[], size_t nvr, const fmiString  value[]){
  setStringParam params = {vr[0], value[0]};
  fmiStatus status;
  sendParamsAndGetStatus(setString, &params, sizeof(params), &status, sizeof(status));

  return status;
}

/***************************************************
Functions for FMI for Co-Simulation
****************************************************/

/* Creation and destruction of slave instances and setting debug status */
fmiComponent fmiInstantiateSlave(fmiString instanceName, fmiString fmuGUID, fmiString fmuLocation, fmiString mimeType, fmiReal timeout, fmiBoolean visible, fmiBoolean interactive, fmiCallbackFunctions functions, fmiBoolean loggingOn){
  
  SOCKADDR_IN sin;
  SOCKET sock;
  socklen_t recsize = sizeof(sin);
  
  SOCKADDR_IN csin;
  SOCKET csock;
  socklen_t crecsize = sizeof(csin);

  sock = socket(AF_INET, SOCK_STREAM, 0);
  int option = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

  if(sock != INVALID_SOCKET){
    
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);
    
    int sock_err = bind(sock, (SOCKADDR*)&sin, recsize);

    if(sock_err != SOCKET_ERROR){

      sock_err = listen(sock, 1);

      if(sock_err != SOCKET_ERROR){

	// run the (windows) local manager of the FMU
        // NB : fmuLocation correspond to the location of this proxy. Therefore, we are not using fmuLoction here.
        // We consider that the GUID field of the modelDescription.xml contains the real fmuLocation.
        
        char* manager_args[] = {"/mnt/c/Users/bcamus/Documents/simgrid-FMI/proxy_fmu/Debug/local_fmu_manager.exe", "Simplified_Euro_LV", "file://C:\\Users\\bcamus\\Documents\\Simplified_Euro_LV\\", 0};

        //char* manager_args[] = {"/mnt/c/Users/bcamus/Documents/simgrid-FMI/proxy_fmu/Debug/local_fmu_manager.exe", "PFTestRMS", "file://C:\\Users\\bcamus\\Documents\\powerfactory-fmu-v06-PF2018-SP3-x86\\examples\\rms\\PFTestRMS", 0};

        switch(fork()){
          case -1:
            perror("unable to fork process");
            break;
          case 0:
            closesocket(sock);
            if(execvp(manager_args[0], manager_args)!=0){
              perror("exec");
              exit(666);
            }else{
              exit(0);
            }
            break;
        }

        fmu.socketID = accept(sock, (SOCKADDR*)&csin, &crecsize);
        closesocket(sock);        
 
        fmiStatus status; 
        sendParamsAndGetStatus(instantiate, 0, 0, &status, sizeof(status));
        if(status ==fmiOK){
          return &fmu;
	}else{
          perror("fmu instantiate error");
	}
      }else{
        perror("listen");
      }
    }else{
      perror("bind");
    }
  }else{
    perror("socket");
  }

  return 0;
}

fmiStatus fmiInitializeSlave(fmiComponent c, fmiReal tStart, fmiBoolean StopTimeDefined, fmiReal tStop){ 
 
 initializeParam params = {tStart, StopTimeDefined, tStop};
 fmiStatus status;
 sendParamsAndGetStatus(initialize, &params, sizeof(params), &status, sizeof(status));

 return status;
}

fmiStatus fmiTerminateSlave(fmiComponent c){

 fmiStatus status;
 sendParamsAndGetStatus(terminateFMU, 0, 0, &status, sizeof(status));
 
 return status;
}
   
fmiStatus fmiResetSlave(fmiComponent c){
 
 fmiStatus status;
 sendParamsAndGetStatus(reset, 0, 0, &status, sizeof(status));

 return status;
}
   
void fmiFreeSlaveInstance(fmiComponent c){
  fmiStatus status;
  sendParamsAndGetStatus(freeFMU, 0, 0, &status, sizeof(status));
  if(status != fmiOK)
    perror("free fmu");
}

fmiStatus fmiSetRealInputDerivatives(fmiComponent c, const  fmiValueReference vr[], size_t nvr, const  fmiInteger order[], const  fmiReal value[]){
  printf("WARNING: fmiSetRealInputDerivatives not implemented");
  return fmiOK;
}

fmiStatus fmiGetRealOutputDerivatives(fmiComponent c, const   fmiValueReference vr[], size_t  nvr, const   fmiInteger order[], fmiReal value[]){
  printf("WARNING: fmiGetRealOutputDerivatives not implemented");
  return fmiOK;
}

fmiStatus fmiCancelStep(fmiComponent c){

  fmiStatus status;
  sendParamsAndGetStatus(cancelStep, 0, 0, &status, sizeof(status));

  return status;
}
   
fmiStatus fmiDoStep(fmiComponent c, fmiReal currentCommunicationPoint, fmiReal communicationStepSize, fmiBoolean newStep){

  doStepParam params = {currentCommunicationPoint, communicationStepSize, newStep};
  fmiStatus status;
  sendParamsAndGetStatus(doStep, &params, sizeof(params), &status, sizeof(status));

  return status;
}

fmiStatus fmiGetStatus(fmiComponent c, const fmiStatusKind s, fmiStatus* value){
  printf("WARNING: fmiGetStatus not implemented !");
  return fmiOK;
}
   
fmiStatus fmiGetRealStatus(fmiComponent c, const fmiStatusKind s, fmiReal* value){
  printf("WARNING: fmiGetRealStatus not implemented !");
  return fmiOK;
}
   
fmiStatus fmiGetIntegerStatus(fmiComponent c, const fmiStatusKind s, fmiInteger* value){
  printf("WARNING: fmiGetIntegerStatus not implemented !");
  return fmiOK;
}
   
fmiStatus fmiGetBooleanStatus(fmiComponent c, const fmiStatusKind s, fmiBoolean* value){
  printf("WARNING: fmiGetBooleanStatus not implemented !");
  return fmiOK;
}
   
fmiStatus fmiGetStringStatus (fmiComponent c, const fmiStatusKind s, fmiString* value){
  printf("WARNING: fmiGetStringStatus not implemented !");
  return fmiOK;
}

