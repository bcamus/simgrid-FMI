#ifdef LOCAL_FMU_MANAGER
  #include <fmiModelTypes.h>
  #include <FMIVariableType.h>
#else
  #include "fmiFunctions.h"
#endif

typedef enum{
  getReal,
  getInteger,
  getBoolean,
  getString,
  setReal,
  setInteger,
  setBoolean,
  setString,
  instantiate,
  initialize,
  terminateFMU,
  reset,
  freeFMU,
  //setRealDer,
  //getRealDer,
  cancelStep,
  doStep,
  //getStatus,
  //getRealStatus,
  //getIntegerStatus,
  //getBooleanStatus,
  //getStringStatus
} callType;

typedef struct{
  fmiValueReference ref;
  fmiReal value;
}setRealParam;

typedef struct{
  fmiValueReference ref;
  fmiInteger value;
}setIntegerParam;

typedef struct{
  fmiValueReference ref;
  fmiBoolean value;
}setBooleanParam;

typedef struct{
  fmiValueReference ref;
  fmiString value;
}setStringParam;

typedef struct{
  fmiStatus status;
  fmiReal value;
}getRealReturn;

typedef struct{
  fmiStatus status;
  fmiInteger value;
}getIntegerReturn;

typedef struct{
  fmiStatus status;
  fmiBoolean value;
}getBooleanReturn;

typedef struct{
  fmiStatus status;
  fmiString value;
}getStringReturn;

typedef struct{
  fmiReal currentTime;
  fmiReal commStep;
  fmiBoolean newStep;
}doStepParam;

typedef struct{
  fmiString instanceName;
  fmiString fmuGUID;
  fmiString fmuLocation;
  fmiString mimeType;
  fmiReal timeout;
  fmiBoolean visible;
  fmiBoolean interactive;
  fmiBoolean loogingOn;
}instantiateParam;

typedef struct{
  fmiReal tStart;
  fmiBoolean stopTimeDefined;
  fmiReal tStop;
}initializeParam;
